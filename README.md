# threat_and_democracy

This repository contains public-facing data for my dissertation project on threat and democracy.

## Files
- threat_final_as_fielded.qsf. This is the qualtrics file for the survey, with consent and debrief removed. 